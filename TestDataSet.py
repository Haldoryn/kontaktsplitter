import unittest
from dataSet import *

class Test(unittest.TestCase):
    def test_fileParser(self):
        #arrange
        f = "default_gender.txt"
        #act
        items = parseDefaultFile(f)
        #assert
        self.assertEqual(items,["male","female","diverse"])

    def test_justLastName(self):
        #arrange
        Base.metadata.drop_all(bind=engine)
        Base.metadata.create_all(bind=engine)
        lastName = "Müller"
        #act
        addPerson(lName=lastName,correctDetection=False)
        #assert
        self.assertEqual(session.query(Person).filter(Person.last_name == "Müller").count(),1)
        self.assertEqual(session.query(Person).filter(Person.last_name == "Müller").first().wasDetectedCorrectly,False)

    def test_allData(self):
        #arrange
        Base.metadata.drop_all(bind=engine)
        Base.metadata.create_all(bind=engine)
        initGenderization()
        #act
        addPerson(lName="Müller",correctDetection=True,fName="Bernd",titles=["Prof.","Dr."],gender=1,salutation="Herr",lSalutation="Sehr geehrter Herr Müller")
        #assert
        p = session.query(Person).filter(Person.last_name == "Müller").first()
        self.assertEqual(p.gender_id,1)
        self.assertEqual(p.last_name,"Müller")
        self.assertEqual(p.first_name,"Bernd")
        self.assertEqual(p.salutation,"Herr")
        self.assertEqual(p.wasDetectedCorrectly,True)
        self.assertEqual(p.letter_salutation,"Sehr geehrter Herr Müller")
        # get gender object with backref
        self.assertEqual(p.gender.value,"male")
        # get title objects with backref
        self.assertListEqual(sorted([title.value for title in p.titles]),sorted(['Prof.','Dr.']))

    
    
if __name__ == '__main__':
    unittest.main()