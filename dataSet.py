from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from datetime import datetime, timedelta
from sqlalchemy import Table, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Enum
from sqlalchemy import Text
from sqlalchemy import Boolean
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKeyConstraint
import sys


Base = declarative_base()

#for prettier gender definition
class GenderEnum(Enum):
    male = 1
    female = 2
    divers = 3

############ TABLES ##################
class Gender(Base):
   __tablename__ = 'gender'
   id = Column(Integer, primary_key = True)
   value = Column(String,unique=True)

association_table = Table('title_person', Base.metadata,
    Column('title_id', Integer, ForeignKey('title.id')),
    Column('person_id', Integer, ForeignKey('person.id'))
)

class Title(Base):
    __tablename__ = 'title'
    id = Column(Integer, primary_key = True)
    value = Column(String)
    person = relationship(
        "Person",
        secondary=association_table,
        back_populates="titles")


class Person(Base):
    __tablename__ = 'person'
    id      =   Column(Integer, primary_key=True)
    salutation = Column(Text,nullable=True)
    letter_salutation = Column(Text,nullable=True)
    gender_id = Column(Integer, ForeignKey('gender.id'))
    first_name = Column(Text,nullable=True)
    last_name = Column(Text,nullable=False)
    wasDetectedCorrectly = Column(Boolean,nullable=False)
    royalTitle = Column(Text,nullable=True)
    gender = relationship("Gender",backref=backref('gender_id', uselist=False))
    titles = relationship("Title",secondary=association_table, back_populates="person")

############ TABLES END ##################

#access Database file and create Session
engine = create_engine('sqlite:///database.db')
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

# get list of values from a default file by its filename
def parseDefaultFile(fileName):
    defStr = open(fileName,"r").read()
    return defStr.split(";")

#add the default gender
def initGenderization():
    listOfGenders = parseDefaultFile("default_gender.txt")
    numOfGenders = session.query(Gender).count()
    if numOfGenders is 0:
        for gender in listOfGenders:
            add(Gender(value=gender))

# add a person
# lastName and correctDetection is required
# the rest is optional
def addPerson(lName,correctDetection,royalTitle=None,titles=None,fName=None,gender:GenderEnum=None,lSalutation=None,salutation=None,title=None):
    person = Person()
    person.first_name = fName
    person.last_name = lName
    person.gender_id = gender
    person.letter_salutation = lSalutation
    person.salutation = salutation
    person.wasDetectedCorrectly = correctDetection
    person.royalTitle = royalTitle
    if titles is not None:
        for title in titles:
            existing_title = session.query(Title).filter(Title.value==title).first()
            if existing_title is not None:
                person.titles.append(existing_title)
            elif title is not '':
                person.titles.append((Title(value=title)))
    add(person)

#generic add function
def add(item):
    global session
    session.add(item)
    session.commit()






