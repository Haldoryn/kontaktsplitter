import PySimpleGUI as sg
import csv
import os
import re

# returns all titles from file
def getTitlesFromFile(file):
    alltitles = []
    with open(file) as csvfile:
            config_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in config_reader:
                for word in row:
                    alltitles += [str(word).rstrip(',').lstrip(' ')]
    return alltitles

# returns all titles corresponding to the nationality
def getTitles(nationality):
    titles = list()
    if nationality is None or nationality is '':
        for file in os.listdir("./configs/"):
            if re.search("^titles_?[a-z]*.txt", file):
                titles.append(getTitlesFromFile("./configs/" + file))
    else:
        titles.append(getTitlesFromFile('./configs/titles_' + nationality + '.txt'))
        titles.append(getTitlesFromFile('./configs/titles.txt'))
    # in this line we flatten the list (titles) to one list and remove the duplicates by creating a dictionary of it then -> return as list
    return list(dict.fromkeys([item for sublist in titles for item in sublist]))  # how to write unreadable code level python

# toggles the visibility of the inputs
def showNewTitleInput(window):
    window.FindElement('_new_title_input').Update(visible=True)
    window.FindElement('_new_title_submit').Update(visible=True)
    window.FindElement('_add_new_title').Update(visible=False)
    window.Refresh()

# returns a layout for the UI with the titles and markes the selected titles
def makeLayout(titels, titleinput,selected):
    layout = [[]]
    nr = 0
    for title in titels:
        if title in selected:
            layout[-1] += [sg.Button(title, key=title, button_color=('Black','#72bcd4'))]
        else:
             layout[-1] += [sg.Button(title, key=title, button_color=('Black','White'))]
        nr += 1
        if(nr % 6 == 0):
            layout.append([])
    layout += [[sg.Input(key='_new_title_input',visible=False)] + [sg.Button('Hinzufügen', key='_new_title_submit',visible=False,bind_return_key=True)]]
    layout[-1] += [sg.Button('+ Neuer Titel', key='_add_new_title',visible=True)]
    layout += [[sg.Ok()]]
    return layout

# Adds title to the file corresponding to the nationality
def addTitle(title, nation):
    if title in getTitles(nation):
        return False
    name = 'titles'
    if nation != '':
        name = name + '_' # naming convention of files (if we have a nation seperate it by _ )
    with open('configs/'+ name + nation + '.txt', "a+") as file:
        if os.stat('configs/'+ name + nation + '.txt').st_size == 0:
            file.write(title)
        else:    
            file.write(', ' + title)
    return True

# showes popup that asks user if he wants to save the title int the file corresponding to the nationality
def wonnaSaveTitles(titles, nation):
    filename = "titles"
    if nation is not "":
        filename += '_'
    layout = [[sg.Text("Folgende Titel sind uns noch nicht bekannt:")],
            [sg.Text(", ".join(titles))],
            [sg.Text("Sollen diese Titel zu configs/" + filename + nation + ".txt hinzugefügt werden?")],
            [sg.Button('Speichern'), sg.Button('Nicht Speichern und Fortfahren')]]
    window = sg.Window('Titel Speichern?').Layout(layout)
    event, values = window.Read()
    window.Close()
    return event

# saves all titles that are not already saved to the file corresponding to the nationality
def saveAllNewTitles(titles, nation):
    oldTitles = getTitles(nation)
    for title in titles:
        if title not in oldTitles:
            addTitle(title, nation)

# starts the titlepicker
def selectTitle(titels,nation,currentTitles):
    notInTitleList = []
    for currentTitle in currentTitles:
        if currentTitle not in titels and currentTitle.strip() is not '':
            notInTitleList.append(currentTitle)
    print(notInTitleList)
    if notInTitleList and wonnaSaveTitles(notInTitleList, nation) is 'Speichern':
        for title in notInTitleList:
            addTitle(title, nation)
            titels.append(currentTitle)

    layout = makeLayout(titels, False,currentTitles)
    window = sg.Window('Titel Auswahlfenster').Layout(layout)
    selectedTitles = currentTitles
    print(selectedTitles)

    while True:
        event, values = window.Read()
        if event is 'Ok' or event is None:
            window.Close()
            return selectedTitles
        elif event is '_add_new_title':
            showNewTitleInput(window)
        elif event is '_new_title_submit':
            if values['_new_title_input'] == '':
                continue
            if addTitle(values['_new_title_input'], nation):
                titels.append(values['_new_title_input'])
            else:
                sg.Popup("Der Titel existiert bereits!")    
            layout = makeLayout(titels, False,selectedTitles)
            window.Close()
            window = sg.Window('Titel Auswahlfenster').Layout(layout)
        else:
            if event in selectedTitles:
                selectedTitles.remove(event)
                window.FindElement(event).Update(button_color = ('Black', 'White'))
            else:
                selectedTitles += [event]
                window.FindElement(event).Update(button_color = ('Black', '#72bcd4'))