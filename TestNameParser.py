import unittest
from NameParser import *


class TestNameParser(unittest.TestCase):
    np = NameParser()

    def test_given_ger_names(self):
        name = self.np.parse_name("Frau Sandra Berger")
        self.assertEqual([], name["title"])
        self.assertEqual("Sandra", name["first"])
        self.assertEqual("Berger", name["last"])
        self.assertEqual("Frau", name["salutation"])
        self.assertEqual("Sehr geehrte Frau", name["letterSalutation"])
        self.assertEqual("ger", name["nation"])

        name = self.np.parse_name("Herr Dr. Sandro Gutmensch")
        self.assertEqual(["Dr."], name["title"])
        self.assertEqual("Sandro", name["first"])
        self.assertEqual("Gutmensch", name["last"])
        self.assertEqual("Herr", name["salutation"])
        self.assertEqual("Sehr geehrter Herr Dr.", name["letterSalutation"])
        self.assertEqual("ger", name["nation"])

        name = self.np.parse_name("Frau Sandra Berger")
        self.assertEqual("Sandra", name["first"])
        self.assertEqual("Berger", name["last"])
        self.assertEqual("Frau", name["salutation"])
        self.assertEqual("ger", name["nation"])

        name = self.np.parse_name("Professor Heinrich Freiherr vom Wald")
        self.assertEqual(["Professor"], name["title"])
        self.assertEqual("Heinrich", name["first"])
        self.assertEqual("Freiherr vom", name["middle"])
        self.assertEqual("Freiherr", name["royal-title"])
        self.assertEqual("Wald", name["last"])
        self.assertEqual("Sehr geehrter Herr Professor", name["letterSalutation"])
        self.assertEqual("Herr", name["salutation"])
        self.assertEqual("ger", name["nation"])

        name = self.np.parse_name("Frau Prof. Dr. rer. nat. Maria von Leuthäuser-Schnarrenberger")
        self.assertEqual(["Prof.", "Dr. rer. nat."], name["title"])
        self.assertEqual("Maria", name["first"])
        self.assertEqual("von Leuthäuser-Schnarrenberger", name["last"])
        self.assertEqual("Frau", name["salutation"])
        self.assertEqual("Sehr geehrte Frau Prof. Dr.", name["letterSalutation"])
        self.assertEqual("ger", name["nation"])

        name = self.np.parse_name("Herr Dipl. Ing. Max von Müller")
        self.assertEqual(["Dipl. Ing."], name["title"])
        self.assertEqual("Max", name["first"])
        self.assertEqual("von Müller", name["last"])
        self.assertEqual("Herr", name["salutation"])
        self.assertEqual("ger", name["nation"])

        # name = self.np.parse_name("Dr. Russwurm, Winfried")  # FIXME (maybe use input validation on ',')
        name = self.np.parse_name("Dr. Winfried Russwurm")
        self.assertEqual(["Dr."], name["title"])
        self.assertEqual("Winfried", name["first"])
        self.assertEqual("Russwurm", name["last"])

        name = self.np.parse_name("Herr Dr.-Ing. Dr. rer. nat. Dr. h.c. mult. Paul Steffens")
        self.assertEqual(["Dr.-Ing.", "Dr. rer. nat.", "Dr. h.c. mult."], name["title"])
        self.assertEqual("Paul", name["first"])
        self.assertEqual("Steffens", name["last"])
        self.assertEqual("Herr", name["salutation"])
        self.assertEqual("Sehr geehrter Herr Dr.", name["letterSalutation"])
        self.assertEqual("ger", name["nation"])

    def test_specific_names(self):
        name = self.np.parse_name("Graf Vlad Dracula")
        self.assertEqual("Graf", name["royal-title"])
        self.assertEqual("Vlad", name["first"])
        self.assertEqual("Dracula", name["last"])

        name = self.np.parse_name("Doc. Howard Phillips Lovecraft")
        self.assertEqual(["Doc."], name["title"])
        self.assertEqual("Howard", name["first"])
        self.assertEqual("Phillips", name["middle"])
        self.assertEqual("Lovecraft", name["last"])

        name = self.np.parse_name("Doc. Bernd \"Schatz Nein\"")
        self.assertEqual(["Doc."], name["title"])
        self.assertEqual("Bernd", name["first"])
        self.assertEqual("", name["middle"])
        self.assertEqual("Schatz-Nein", name["last"])

        name = self.np.parse_name("Foobar")
        self.assertEqual("", name["first"])
        self.assertEqual("Foobar", name["last"])

    def test_get_gender(self):
        # salutation > guess from genderize
        name = self.np.parse_name("Herr Lisa Simpson")
        self.assertEqual("male", name["gender"])
        self.assertEqual("ger", name["nation"])

        name = self.np.parse_name("Lisa Simpson")
        self.assertEqual("female", name["gender"])

    def test_reevaluate(self):
        json_body = {
            "title": ["Prof."],
            "first": "Lisa",
            "middle": "",
            "last": "Simpson",
            "suffix": "",
            "nation": "ger",
            "salutation": "salut",
            "gender": "female",
            "letterSalutation": "",
            "royal-title": ""
        }
        # get salutation and letterSalutation by gender and nation
        name = self.np.re_evaluate(json_body)
        self.assertEqual("Sehr geehrte Frau Prof.", name["letterSalutation"])
        self.assertEqual("Frau", name["salutation"])


if __name__ == '__main__':
    unittest.main()
