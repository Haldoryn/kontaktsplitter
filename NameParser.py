from nameparser import HumanName
from nameparser.config import CONSTANTS
from genderize import Genderize
from MappingDictionaries import *
import csv
import os
import re

# Class that contains all functionality needed to parse a given string into single name components
class NameParser:
    loaded_salutations = dict()
    loaded_titles = dict()
    loaded_title_parts = list()
    loaded_royal_titles = dict()

    def __init__(self):
        self.load_configs()

    # swap key and value in the dictionary
    def reverse(self,dic):
        return dict((v,k) for k,v in dic.items())

    # clears the loaded configs and rereads it
    def reload_configs(self):
        self.loaded_salutations = dict()
        self.loaded_titles = dict()
        self.loaded_title_parts = list()
        self.loaded_royal_titles = dict()
        self.load_configs() # refresh title list

    # if there is " " in name concat it by -
    def concatManyMarkedLastNames(self,full_name):
        if '\"' in full_name:
            last = re.search("\"[a-zA-Z]+( [a-zA-Z]+)*\"", full_name)
            if last is not None:
                formatted_last = str(last.group(0)).lstrip('\"').rstrip('\"').replace(" ","-")
                full_name = full_name.replace(last.group(0), formatted_last)
            return full_name
        return full_name    

    # main function to parse a full name given as a single string
    # returns all parts of the given name as a json object
    def parse_name(self, full_name):
        self.reload_configs()
    
        full_name = self.concatManyMarkedLastNames(full_name)
        name = HumanName(full_name)
        name = self.ensureOneWordIsLastName(name)
        
        title = self.get_cleaned_title(name)
        nation = self.try_to_get_nation_from_name(name)
        salutation = self.get_salutation(name)
        gender = self.get_gender(name,nation,salutation)
        letterSalutation = self.get_letter_salutation(gender,title,nation)
        royalTitle = self.get_royal_title(name)

        # if there is no salutation but gender from genderize and we know the nation (by title) -> get salutation by nation and gender
        if gender is not None and nation is not "":
            salutation = self.get_salutation_based_on_gender_nation(gender,nation)

        json_body = {
            "title": title,
            "first": name.first,
            "middle": name.middle,
            "last": name.last,
            "suffix": name.suffix,
            "nation": nation,
            "salutation": salutation,
            "gender":gender,
            "letterSalutation": letterSalutation,
            "royal-title": royalTitle
        }
        print(json_body)
        return json_body

    # if the full name only consists of a single word, ensure it to be the last name
    def ensureOneWordIsLastName(self, name):
        if name.last is "" and name.first is not "":
            name.last = name.first
            name.first = ""
        return name

    # reevaluate a previously evaluated name
    # this function is used if one of the identified components was edited by the user
    def re_evaluate(self,person):
        salut = ""
        lSalut = ""
        if person['gender'] is not None  and person['nation'] is not "":
           salut =  self.get_salutation_based_on_gender_nation(person['gender'],person['nation'])
           lSalut = self.get_letter_salutation(person['gender'],person['title'],person['nation'])
        json_body = {
            "title": person['title'],
            "first": person['first'],
            "middle": person['middle'],
            "last": person['last'],
            "suffix": person['suffix'],
            "nation": person['nation'],
            "salutation": salut,
            "gender":person['gender'],
            "letterSalutation": lSalut,
            "royal-title":person["royal-title"]
        }
        return json_body

    # get the correct salutations for the given combination of gender and nationality
    def get_salutation_based_on_gender_nation(self,gender,nation):
        # germany
        if nation == "ger":
            # reverse dictionary to get (gender:salutation) instead of (salutation:gender)
            return self.reverse(salut_gender_ger)[gender]
        if nation == "fr":
            # reverse dictionary to get (gender:salutation) instead of (salutation:gender)
            return self.reverse(salut_gender_fr)[gender]
        if nation == "gb":
            return self.reverse(salut_gender_gb)[gender]
        # TODO: rules for other languages

    # try to identify the gender of the person by their salutation and nationality or by their name
    def get_gender(self, name,nation,salutation):
        # first try to get gender from salutation
        result = self.get_gender_based_on_salutation(nation,salutation)
        if result is not None:
               return result
        # if still no gender detected ask genderize service
        return self.get_gender_based_on_firstName(name)

    # call the Genderize service to retrieve the gender by firstName    
    def get_gender_based_on_firstName(self,name):
        try:
            result = Genderize().get([name.first])
        except:
            print("no internet connection")
            return None
        if result[0]["gender"] == None:
            return None
        if result[0]["probability"] > 0.5:
            return result[0]["gender"]
        else:
            return None

    # retrieves the gender by nation and salutation from the MappingDictionaries
    def get_gender_based_on_salutation(self,nation,salutation):
        result = None
        if salutation == "":
            return None
        #germany
        if nation == "ger":
           result =  salut_gender_ger.get(salutation)
        if nation == "fr":
           result =  salut_gender_fr.get(salutation) # example for french language
        if nation == "gb":
           result =  salut_gender_gb.get(salutation)
        # TODO: rules for other languages
        return result

    # retrieves the letterSalutation by title gender and nation from the MappingDictionaries
    def get_letter_salutation(self,gender,title,nation):
        if gender == None:
            return ""
        sal = ""
        # germany
        if nation == "ger":
            addOns = let_salut_addOn_ger 
            print(gender)
            sal = let_salut_ger[gender]
            for addOn in addOns:
                if len([t for t in title if t.startswith(addOn)]) > 0: # if there is for e.g a Dr. rer. nat. -> match 
                    sal = sal + " " + addOn
        #france
        if nation == "fr":
            sal = let_salut_fr[gender]
        if nation == "gb":
            sal = let_salut_gb[gender]
        # TODO: other nations
        return sal

    # remove salutations from title
    def get_cleaned_title(self, name):
        titles = []
        for index in range(len(name.title_list)):
            title_part = name.title_list[index]
            if title_part not in self.loaded_salutations:  # 'not' for support of default titles
                title = self.__find_longest_match(title_part, index,
                                                  name.title_list, self.loaded_titles, None)
                if title is not None:
                    titles.append(title)
        return titles

    # helper function to get the cleaned titles
    # matches title parts to the correct title (ex. "Dr. rer. nat" should not be matched to "Dr.")
    def __find_longest_match(self, title_part, title_part_index, title_list, complete_titles, fallback_title):
        possible_titles = [title for title, value in self.loaded_titles.items() if title.startswith(title_part)]  # return all titles that start with title_part
        if len(possible_titles) > 1:  # title_list contains multiple titles starting with title_part
            if title_part_index < len(title_list)-1:  # there ARE more title_parts available to test longer matches
                extended_title_part = title_part + " " + title_list[title_part_index + 1]
                return self.__find_longest_match(extended_title_part, title_part_index + 1,
                                                 title_list, complete_titles, title_part)
            else:  # there are NO more title_parts available to test longer matches -> title_part is the longest match
                return title_part
        else:  # title_list contains exactly one title starting with title_part OR it is empty
            if len(possible_titles) == 1 and possible_titles[0] in complete_titles:  # only one possible title left -> longest match
                longest_match = possible_titles[0]
                return longest_match
            else:  # recursion went one iteration too far, title_part of last iteration is longest match
                return fallback_title

    # extract the salutations from the other titles
    def get_salutation(self, name):
        salutation = ""
        for title in name.title_list:
            if title in self.loaded_salutations:
                salutation = salutation + " " + title
        return salutation.lstrip(" ")

    # extract the royal titles from the other titles
    def get_royal_title(self, name):
        royal_title = ""
        for title in name.title_list:
            if title in self.loaded_royal_titles:
                royal_title = royal_title + " " + title
        for title in name.middle_list:  # royal titles can be in the midle of the name
            if title in self.loaded_royal_titles:
                royal_title = royal_title + " " + title
        return royal_title.lstrip(" ")

    # try to identify the nation of a person by inspecting their salutations and royal titles
    def try_to_get_nation_from_name(self, name):
        for title in name.title_list:  # look for first part of salutations or royal titles with only one possible nation
            if title in self.loaded_salutations and len(self.loaded_salutations[title]) is 1:
                return self.loaded_salutations[title][0]
            if title in self.loaded_royal_titles and len(self.loaded_royal_titles[title]) is 1:
                return self.loaded_royal_titles[title][0]
        for title in name.middle_list:  # royal titles can be in the middle of the name
            if title in self.loaded_royal_titles and len(self.loaded_royal_titles[title]) is 1:
                return self.loaded_royal_titles[title][0]
        return ""

    # load all titles, salutations and royal titles from all files inside ./configs/
    def load_configs(self):
        salutation_files = list()
        title_files = list()
        royal_title_files = list()
        for file in os.listdir("./configs/"):
            if re.search("^salutations_?[a-z]*.txt", file):
                salutation_files.append(file)
            if re.search("^titles_?[a-z]*.txt", file):
                title_files.append(file)
            if re.search("^royal-titles_?[a-z]*.txt", file):
                royal_title_files.append(file)
        self.__add_salutations(salutation_files)
        self.__add_titles(title_files)
        self.__add_royal_titles(royal_title_files)
        print("initialization complete\n\n")

    # add all salutations contained in salutations files to the local salutations dictionary
    def __add_salutations(self, s_files):
        for file in s_files:
            lang = file.lstrip("salutations").lstrip("_").rstrip(".txt")
            with open("./configs/"+file) as csvfile:
                config_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in config_reader:
                    for word in row:
                        salutation = str(word).rstrip(',').lstrip(" ")
                        CONSTANTS.titles.add(salutation)
                        if salutation in self.loaded_salutations:
                            self.loaded_salutations[salutation].append(lang)
                        else:
                            self.loaded_salutations[salutation] = [lang]
            print("loaded salutations from file: ", "./configs/"+file)

    # add all royal titles contained in royal title files to the local royal title dictionary
    def __add_royal_titles(self, s_files):
        for file in s_files:
            lang = file.lstrip("royal-titles").lstrip("_").rstrip(".txt")
            with open("./configs/"+file) as csvfile:
                config_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in config_reader:
                    for word in row:
                        royal_title = str(word).rstrip(',').lstrip(" ")
                        CONSTANTS.titles.add(royal_title)
                        if royal_title in self.loaded_royal_titles:
                            self.loaded_royal_titles[royal_title].append(lang)
                        else:
                            self.loaded_royal_titles[royal_title] = [lang]
            print("loaded royal titles from file: ", "./configs/"+file)

    # add all titles contained in title files to the local title dictionary
    def __add_titles(self, t_files):
        for file in t_files:
            lang = file.lstrip("titles").lstrip("_").rstrip(".txt")
            with open("./configs/"+file) as csvfile:
                config_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in config_reader:
                    for word in row:
                        title = str(word).rstrip(',').lstrip(' ')
                        if title in self.loaded_titles:
                            self.loaded_titles[title].append(lang)
                        else:
                            self.loaded_titles[title] = [lang]
            with open("./configs/" + file) as csvfile:
                config_reader_parts = csv.reader(csvfile, delimiter=' ', quotechar='|')
                for row in config_reader_parts:
                    for word in row:
                        titlepart = str(word).rstrip(',')
                        CONSTANTS.titles.add(titlepart)
                        self.loaded_title_parts.append(titlepart)
            print("loaded titles from file: ", "./configs/"+file)
