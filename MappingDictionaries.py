# germany
salut_gender_ger = { "Herr":"male","Frau":"female",None:"","":"diverse"}
let_salut_addOn_ger = ["Prof.","Dr.","Professor"]
let_salut_ger = {"male":"Sehr geehrter Herr","female":"Sehr geehrte Frau","diverse":"Guten Tag",None:""}

# french
salut_gender_fr= { "M":"male","Mme":"female","":"diverse",None:""}
let_salut_fr = {"male":"Monsieur","female":"Madame","diverse":"Bonjour",None:""}

# great britain
salut_gender_gb= { "Mr":"male","Mrs":"female","Ms":"female","":"diverse",None:""}
let_salut_gb = {"male":"Dear","female":"Dear","diverse":"Dear",None:""}

# variable to readable output
var_to_read = {"firstname":"Vorname",
                "lastname":"Nachname",
                "gender":"Geschlecht",
                "title":"Titel",
                "salutation":"Anrede",
                "lettersalutation":"Briefanrede"}