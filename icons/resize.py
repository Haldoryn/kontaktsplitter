from PIL import Image
from resizeimage import resizeimage 
 
with open('images.png', 'r+b') as f:
    with Image.open(f) as image:
        cover = resizeimage.resize_cover(image, [64, 64])
        cover.save('select64.png', image.format)
