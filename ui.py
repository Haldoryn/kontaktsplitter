import PySimpleGUI as sg
import dataSet as ds
import popups as pop
import titleSelect as tit
from NameParser import *

# init Database and NameParser
ds.initGenderization()
np = NameParser()
currentPerson = None
fieldsEnabled = False
hasChanged = False
enDeMap = {
    "male":"Männlich",
    "female":"Weiblich",
     None:"Nicht Angegeben",
    "diverse":"Divers"
    }

genderDic = {
    'Nicht Angegeben': None,
    'Männlich': 1,
    'Weiblich': 2,
    'Divers': 3
}

# only nations that are supported ; see -> MappingDictionaries
nationDic = {
    "" : "select",
    "ger" : "germany",
    "fr" : "france",
    "gb":"united-kingdom"
}

titleDic = { 'Nicht Angegeben': None }

def reverse(dic):
    return dict((v,k) for k,v in dic.items())

# wrapper to find elements in the window
def find(element):
    return window.FindElement(element)

# reset all values in the fields and disabel them
def resetFields():
    global hasChanged
    hasChanged = False
    global fieldsEnabled
    fieldsEnabled = False
    find('firstname').Update(disabled=True, value = '')
    find('lastname').Update(disabled=True, value = '')
    find('gender').Update(disabled=True, set_to_index=0)
    find('title').Update(disabled=True, value='')
    find('selectTitle').Update(disabled=True)
    find('salutation').Update(disabled=True, value = '')
    find('lettersalutation').Update(disabled=True, value = '')
    find('name').Update(value = '')
    find('gender').Update(disabled=True, value = '')
    find('royaltitle').Update(disabled=True, value = '')
    find('selectTitle').Update(disabled=True)
    find('nationality').Update(disabled=True, image_filename='./icons/select.png')



# fill the fields with a parsed person object
def setFields(person):
    global fieldsEnabled
    fieldsEnabled = True
    find('firstname').Update(disabled=False, value = person["first"])
    find('lastname').Update(disabled=False, value = person["last"])
    find('title').Update(disabled=False, value=join(person["title"]))
    find('selectTitle').Update(disabled=False)
    find('salutation').Update(disabled=False, value = person["salutation"])
    find('gender').Update(disabled=False, value=enDeMap[person["gender"]])
    find('lettersalutation').Update(disabled=False, value = person["letterSalutation"])
    find('royaltitle').Update(disabled=False, value = person["royal-title"])
    find('selectTitle').Update(disabled=False)
    find('nationality').Update(disabled=False, image_filename='./icons/' + nationDic[person["nation"]] + '.png')


# check if all needed values are present
def hasAllAttributes(values):
    if len(getKeysOfEmptyFields(values)) == 0:
            return True
    return False

# returns all keys of the elements in the window that are obligatory and empty
def getKeysOfEmptyFields(values):
    emptyFields = []
    for item in values:
       if item != 'royaltitle' and item != 'name' and item != 'title':
            if values[item] is '' or values[item] == 'Nicht Angegeben':
                emptyFields.append(item)
    return emptyFields

# own join functionality to get prettier output
def join(lst):
    ret = ""
    for el in lst:
        ret = ret + el + ", "
    return ret[0:len(ret)-2]

# colors all field_descriptions of the given fields
def color(fields,color):
    if fields is None or len(fields) == 0:
        return
    for field in fields:
        if field != 'name':
            find('t_'+field).Update(background_color=color)


# if the user pressed a flag
def nationalitySelected(nat,person):
        newNat = reverse(nationDic).get(nat)
        if newNat is None:
            sg.Popup('Noch nicht unterstützt',
            selectedNationality ) 
        else: 
            person['nation'] = newNat
            setFields(np.re_evaluate(person))
            find('nationality').Update(image_filename='./icons/' + nat + '.png')

# parses string in name field and showes results in the edit section 
def check():
    person = np.parse_name(find('name').Get())
    setFields(person)
    global currentPerson
    currentPerson = person

# saves current person in the DB
def save():
    currentTitles = []
    if values["title"].strip() != '':
        currentTitles = values["title"].split(", ")
    tit.saveAllNewTitles(currentTitles, currentPerson['nation'])
    ds.addPerson(lName=values['lastname'],
                    correctDetection=hasAllAttributes(values),
                    fName=values['firstname'],
                    gender=genderDic[values["gender"]], 
                    titles= values["title"].split(", "), # TODO: ask Marcel how he wants to save it
                    lSalutation=values['lettersalutation'],
                    royalTitle=values['royaltitle'],
                    salutation=values['salutation'])
    resetFields()
    sg.Popup("Der Datensatz wurde gespeichert!")    

def saveOrCheck():
    event = pop.saveOrCheckPopup()
    if event is 'save':
        save()
    if event is 'check':
        check()

# UI definition
editField = [[sg.Text('Vorname*:',size=(10, 1),key='t_firstname'), sg.Input(key='firstname',disabled=True,change_submits=True)],
            [sg.Text('Nachname*:',size=(10, 1),key='t_lastname'), sg.Input(key='lastname',disabled=True,change_submits=True)],
            [sg.Text('Geschlecht*:',size=(10, 1),key='t_gender'), sg.DropDown(['Nicht Angegeben','Männlich', 'Weiblich', 'Divers'],key="gender",disabled=True,change_submits=True)],
            [sg.Text('Titel:',size=(10, 1),key='t_title'), sg.Input(key='title',disabled=True,change_submits=True), sg.Button(key='selectTitle', button_text='Auswahl',disabled=True)],
            [sg.Text('Anrede*:',size=(10, 1),key='t_salutation'), sg.Input(key='salutation',disabled=True,change_submits=True)],
            [sg.Text('Briefanrede*:',size=(10, 1),key='t_lettersalutation'), sg.Input(key='lettersalutation',disabled=True,change_submits=True)],
            [sg.Text('Adelstitel:',size=(10, 1),key='t_royaltitle'), sg.Input(key='royaltitle',disabled=True,change_submits=True)],
            [sg.Text('Nationalität:', size=(10,1)), sg.Button(key='nationality', image_filename='./icons/select.png', image_size=(64,64),disabled=True)]]

layout = [[sg.Text('Name:',size=(10, 1)), sg.InputText(key = 'name', focus=True), sg.Submit("Prüfen")],
            [sg.Frame("Bearbeiten", editField, key='Edit')], [sg.ReadButton("Speichern")]]

window = sg.Window('Kontaktsplitter').Layout(layout).Finalize()

selectedNationality = None

while True:
    event, values = window.Read()    
    color(values,"#F0F0F0") 
    if event is None:
        break
    if event is 'Prüfen':
        if hasChanged:
            saveOrCheck()
        else:
            check()

    if event is 'Speichern':
        save()

    if event is 'nationality':
        selectedNationality = pop.selectNationality()    
        if selectedNationality is not None and currentPerson is not None:
            nationalitySelected(selectedNationality,currentPerson)
            hasChanged = True

    if event is 'selectTitle':
        hasChanged = True
        alltitles = []
        currentTitles = []
        if values["title"].strip() != '':
            currentTitles = values["title"].split(", ")
        titels = tit.selectTitle(tit.getTitles(currentPerson['nation']),currentPerson['nation'],currentTitles)
        find('title').Update(value= join(titels))
        currentPerson["title"] = titels
        setFields(np.re_evaluate(currentPerson))
    if currentPerson is None :
        continue
    if event is 'gender':
        currentPerson['gender'] = reverse(enDeMap)[values['gender']]
        setFields(np.re_evaluate(currentPerson))
        hasChanged = True
    if event is 'title':
        currentPerson['title'] = [values['title']]
        setFields(np.re_evaluate(currentPerson))
        hasChanged = True
    if event is 'firstname':
        currentPerson['first'] = values['firstname']
        hasChanged = True
    if event is 'lastname':
        currentPerson['last'] = values['lastname']
        hasChanged = True
    if event is 'royaltitle' or event is 'lettersalutation' or event is 'salutation':
        hasChanged = True
    if fieldsEnabled:
        # reread updated values
        event, values = window.ReadNonBlocking()
        # color the empty fields
        color(getKeysOfEmptyFields(values),'orange') 
