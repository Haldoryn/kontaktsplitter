import PySimpleGUI as sg

def selectNationality():
    layout = [[sg.Button(key='germany', image_filename='./icons/germany.png', image_size=(64,64)),
                    sg.Button(key='france', image_filename='./icons/france.png', image_size=(64,64)),
                    sg.Button(key='italy', image_filename='./icons/italy.png', image_size=(64,64)),
                    sg.Button(key='spain', image_filename='./icons/spain.png', image_size=(64,64)),
                    sg.Button(key='united-kingdom', image_filename='./icons/united-kingdom.png', image_size=(64,64)),
                    sg.Button(key='select', image_filename='./icons/select.png', image_size=(64,64))]]
    window = sg.Window('Nationalitätsauswahlfenster').Layout(layout)
    event, values = window.Read()
    window.Close()
    return event

def saveOrCheckPopup():
    layout = [[sg.Text('Sie haben ungespeicherte Änderungen')],
            [sg.Text('Wollen Sie die Änderungen in die Datenbank abspeichern, oder den Namen neu Überprüfen')],
            [sg.Button('Speichern', key='save'), sg.Button('Überprüfen', key= 'check')]]
    window = sg.Window('Speichern oder Überprüfen').Layout(layout)
    event, values = window.Read()
    window.Close()
    return event    
